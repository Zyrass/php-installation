# Installer un echo-système PHP sur Ubuntu 20+

<br />

<div align="center">
      <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1200px-PHP-logo.svg.png" width="150" loading="lazy" />
      <img src="https://www.sculpteo.com/blog/wp-content/uploads/2016/07/ubuntu-grand-logo1.png" width="150" loading="lazy" />
</div>

<br />


|Version du guide|Auteur|Source|
|:------:|:------:|:------:|
|**3.2.2**|**Alain Guillon** alias "**Zyrass**" ( :fr: )|**Alex32** ( :fr: )|

        /!\ - Toutes les explications qui vont suivre sont lié à "Ubuntu 20+".
        /!\ - Vous êtes libre d'utiliser toutes ces explications au cas où, si vous rencontrez un quelconque problème particulier.

<br />

## Sommaire principal

Je vous propose ici, 2 sections :
      
- **un premier tableau** regroupant différents liens afin de vous permettre de vous focaliser essentiellement sur **les indispensables de PHP**.  
- dans **le second tableau**, vous retrouverez comment installer **des frameworks** liés à **PHP**. 

<br />

## I - Indispensable

> Ici sont répertoriés toutes les techniques qui vont nous permettent d'être opérationnel en PHP.


| Chapitre | Technologie | Description | Lien | Statut |
| :--------: | :--------: | -------- | :--------: | :--------: |
| I | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Apache_HTTP_server_logo_%282016%29.svg/1280px-Apache_HTTP_server_logo_%282016%29.svg.png" width="50" loading="lazy" /> | Installation d'un serveur **apache** afin d'interpréter **PHP**.  | [:link:](indispensable/01-apache2/readme.md) | :heavy_check_mark:
| II | <img src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/01/MariaDB_610.png" width="50" loading="lazy" /> | Installation de **MariaDB** pour nous permettre d'utiliser **MySQL**.  | [:link:](indispensable/02-mariadb/readme.md)| :heavy_check_mark:
| III | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png" width="50" loading="lazy" /> | Liens vers une passerelle pour voir plusieurs installations de **PHP**. | [:link:](indispensable/03-php/readme.md) | :heavy_check_mark:
| IV |<img src="https://upload.wikimedia.org/wikipedia/commons/9/95/PhpMyAdmin_logo.png" width="50" loading="lazy" /> | Installation de **phpMyAdmin** pour gérer nos bases de données **MySQL**.  | [:link:](indispensable/04-phpmyadmin/readme.md) | :heavy_check_mark:
| V | <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Logo-composer-transparent.png" width="50" loading="lazy" /> | **facultatifs en procédural**.  <br />Installation de composer pour une meilleure utilisation de la POO  | [:link:](indispensable/05-composer/readme.md) | :heavy_check_mark:


<br />

## II - Frameworks

> Ici, pas d'ordre requis, vous êtes libre d'installer le framework PHP que vous voulez.  
> **Mais également, vous êtes aussi libre de ne pas les utiliser**.

| Chapitre | Technologie | Lien | Statut |
| :--------: | :--------: | :--------: | :--------: |
| VI | <img src="https://cdn.worldvectorlogo.com/logos/symfony.svg" width="50" loading="lazy" /> | [:link:](frameworks/01-symfony/readme.md) | :heavy_check_mark: |
| VII | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Laravel.svg/1200px-Laravel.svg.png" width="50" loading="lazy" /> | :heavy_multiplication_x: | __*à faire*__  |

<br />

## Remerciements

Comme je l'ai mentionné au tout début, au niveau des sources,  
pas mal de choses m'auront été expliqué par **Alex32**.  

> **_Je tiens tout personnellement à le remercier pour son implication_**.  

Encore aujourd'hui, il m'explique **pas à pas** ce qu'il faut faire pour **améliorer les installations de PHP**.  
Bien entendu, via **son accord** j'ai repris tout son processus pour avoir une trace personnelle 
 en cas d'une quelconque réinstallation d'un système **Ubuntu 20+**, ce qui m'évite de chercher comment faire.
