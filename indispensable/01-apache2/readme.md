# Chapitre I - Installation d'Apache2

<br />

<div align="center">

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Apache_HTTP_server_logo_%282016%29.svg/langfr-1920px-Apache_HTTP_server_logo_%282016%29.svg.png" width="800" loading="lazy" />

</div>

<br />

**Le logiciel libre Apache HTTP Server est un serveur HTTP créé et maintenu au sein de la fondation Apache**.  
**Jusqu'en avril 2019, ce fut le serveur HTTP le plus populaire du World Wide Web**.  
**Il est distribué selon les termes de la licence Apache.**  
[source wikipédia](https://fr.wikipedia.org/wiki/Apache_HTTP_Server)

<div align=center>

    /!\ Important : ne négligez surtout pas le changement de propriétaire. (1.2)

</div>

<br />

## Sommaire d'Apache

| chapitre | description | lien | statut |
| :--------: | -------- | :--------: | :---------: |
| 1.1 | Installation d'apache2 | [:link:](#11-installation) | :heavy_check_mark:
| 1.2 | Changé le propriétaire | [:link:](#12-chang%C3%A9-le-propri%C3%A9taire-de-la-racine-du-document) | :heavy_check_mark:
| 1.3 | Ré-installation d'apache2 | [:link:](#13-r%C3%A9-installation-dapache2-suite-%C3%A0-une-erreur) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**.


<br />

## 1.1 - Installation

```sh
# 1.1.0 - Avant de commencer on s'assure que tout est OK
sudo apt update && sudo apt full-upgrade && sudo apt autoremove

# 1.1.1 - Installer apache2 et apache2-utils.
sudo apt install -y apache2 apache2-utils

# 1.1.2 - Démarrer apache2 à chaque démarrage de l'ordinateur.
sudo systemctl enable apache2

# 1.1.3 - Vérifier l'état du paquet apache2 une fois celui-ci d'installé.
sudo systemctl status apache2
```
### Quelques **actions** intéressantes :  

```sh
# 1.1.4 - Si apache2 n'est pas démarré (ce qui peut arrivé).
sudo systemctl start apache2

# 1.1.5 - Pour stopper le service apache2.
sudo systemctl stop apache2

# 1.1.6 - Pour redémarrer le service apache2.
sudo systemctl restart apache2
```

Jetez un oeil ici pour avoir plus d'informations sur "**systemctl** : **[Lien](https://www.linuxtricks.fr/wiki/systemd-les-commandes-essentielles)**

<br />

## 1.2 - Changé le propriétaire de la racine du document

> Changé le propriétaire du répertoire **/var/www/html/** est important.  
> En effet c'est assez énervant si on se retrouve dans l'impossibilité de faire quoi que ce soit à cause d'une erreur de droit...  
> surtout si nous sommes sur notre propre ordinateur.


```sh
# 1.2.1 - Définition du user apache comme propriétaire de la racine du document.
sudo chown www-data:www-data /var/www/html/ -R

# 1.2.2 - Petite modification pour accéder à localhost, vu que pour le moment,
#       nous pouvons seulement accéder à l'adresse suivante : 
#       http://127.0.0.1
sudo nano /etc/apache2/conf-available/servername.conf
```

### Ajoutons **localhost** à la place de **127.0.0.1** :

```sh
# 1.2.3 - Dans le fichier qui aura été ouvert via "nano", on ajoutera ceci tel quel :
ServerName localhost
```

    - On enregistre le fichier avec la combinaison : CTRL + S
    - On quitte nano avec cette combinaison de touches : CTRL + X

### Activation des modifications :

```sh
# 1.2.4 - On active la modification que l'on vient de faire via la commande :
sudo a2enconf servername.conf
```
### Redémarrage d'apache :

```sh
# 1.2.5 - On relance le serveur "Apache" :
sudo systemctl reload apache2
```

**Ps** : Si vous êtes intéressé par les **noms virtuels**, voici un [lien](https://httpd.apache.org/docs/2.4/fr/vhosts/examples.html) vers des **exemples d'utilisation de VirtualHost**.

<br />

## 1.3 - Ré-installation d'Apache2 suite à une erreur

Il est possible qu'apache pour **X raison(s)** ne fonctionne plus... _Pas de panique_,  
on va le **désinstaller** et le **réinstaller** proprement juste après.

### Suppression d'apache2 / apache2-utils :
```sh
# 1.3.1 - Pour supprimer correctement apache2 et **apache2-utils**
sudo apt remove apache2 apache2-utils
```

### Explication : 

> Avec ces confirmations, tout devrait se supprimer.  
> En revanche, **pas de panique** si on vous dit que :

    le répertoire « /var/lib/apache2 » n'était pas vide, donc il n'a pas été supprimé

> on regardera l'étape 3.2 justes en dessous.

### Suppression du répertoire résistant :

```sh
# 1.3.2 - Suppression du répertoire existant qui nous pose des problèmes.
sudo rm -rf /var/lib/apache2
```

<br />

## Sommaire principal

> Je vous propose de suivre le lien vers **MariaDB** pour faire en sorte qu'on puisse utiliser **MySQL**.  

> _**Si vous savez ce que vous faites, vous êtes libre d'aller où vous le souhaitez**_.

| Chapitre | Technologie | Ordre conseillé | Lien | Statut |
| :--------: | :--------: | :--------: | :--------: | :--------:
| I | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Apache_HTTP_server_logo_%282016%29.svg/1280px-Apache_HTTP_server_logo_%282016%29.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../01-apache2/readme.md) | :heavy_check_mark:
| II | <img src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/01/MariaDB_610.png" width="50" loading="lazy" /> | :thumbsup: | [:link:](../02-mariadb/readme.md) | :heavy_check_mark:
| III | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../03-php/readme.md) | :heavy_check_mark:
| IV | <img src="https://upload.wikimedia.org/wikipedia/commons/9/95/PhpMyAdmin_logo.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../04-phpmyadmin/readme.md) | :heavy_check_mark:
| V | <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Logo-composer-transparent.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../05-composer/readme.md) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**.
