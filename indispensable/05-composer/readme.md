# Chapitre V - Installation de Composer

<br />

<div align=center>

<img src="https://www.kaherecode.com/uploads/tutorials/5c532bd911c4f.png" width="800" />

<br />

**Composer est un logiciel gestionnaire de dépendances libre écrit en PHP**.  
**Il permet à ses utilisateurs de déclarer et d'installer les bibliothèques dont le projet principal a besoin**.  
**Le développement a débuté en avril 2011 et a donné lieu à une première version sortie le 1ᵉʳ mars 2012**. [source wikipédia](https://fr.wikipedia.org/wiki/Composer_(logiciel))

</div>

<br />

## Sommaire de composer


| chapitre | description | lien | statut |
| :--------: | -------- | :--------: | :----: |
| 5.1 | Installation de composer | [:link:](#51-installation-de-composer) | :heavy_check_mark:
| 5.2 | Lancer composer | [:link:](#52-lancer-composer) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**. 


<br />

## 5.1 - Installation de composer

<br />

```sh
# 5.1.1 - Mettre à jour le cache du gestionnaire de paquets au cas où.
sudo apt update

# 5.1.2 - Installation du paquet "php-cli" et "unzip" qui seront utilisés.
sudo apt install php-cli unzip

# 5.1.3 - Être OBLIGATOIREMENT dans son répertoire HOME/USERNAME. 
#         Au cas où, on peut s'y rendre comme ça :
cd ~

# 5.1.3 bis - Pour être sûr d'être dans le bon répertoire, saisissez la commande "pwd" qui retournera :
pwd
```

<br />

### Moi j'obtiendrai :

    /home/zyrass

<br />

### On reprend l'installation :

```sh
# 5.1.4 - téléchargement de composer
curl -sS https://getcomposer.org/installer -o composer-setup.php
```
<br />

### Vérification de l'intégrité du fichier téléchargé :

<br />

> Nous allons vérifier que **l'installateur téléchargé correspond au hachage SHA-384** pour le dernier installateur trouvé sur la page des clés publiques/signatures de Composer.  

> Pour faciliter l'étape de vérification, vous pouvez utiliser la commande suivante pour obtenir par programmation le dernier hachage de la page de Composer  
> et le stocker dans une variable shell

<br />

```sh
# Vérification du hashage
HASH=`curl -sS https://composer.github.io/installer.sig`

# Si l'on souhaite voir la valeur obtenue :
echo $HASH # Retournera une série de chiffres et de lettres.

# 5.1.5 - Utilisation du script fourni par la page de Composer.
php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
```
<br />

### Message qui devrait s'afficher si aucun problème n'est rencontré :

    Installer verified

<br />

### Poursuite et fin de l'installation :

```sh
# 5.1.6 - Installation de Composer globalement via la commande :
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```
<br />


**Info :** l'option "**--filename=composer**" permettra d'utiliser simplement le nom de **composer** dans le terminal au lieu de **php /usr/local/bin/composer**

<br />

### Résultat attendu :

    All settings correct for using Composer
    Downloading...

    Composer (version 2.0.8) successfully installed to: /usr/local/bin/composer
    Use it: php /usr/local/bin/composer

<br />

## 5.2 - Lancer composer

<br />

```sh
# 5.2.1 - Testons si tout fonctionne, dans le terminal on saisira : 
composer
```

<br />

### Retour final dans le terminal :

<br />

```sh
   ______
  / ____/___  ____ ___  ____  ____  ________  _____
 / /   / __ \/ __ `__ \/ __ \/ __ \/ ___/ _ \/ ___/
/ /___/ /_/ / / / / / / /_/ / /_/ (__  )  __/ /
\____/\____/_/ /_/ /_/ .___/\____/____/\___/_/
                    /_/
Composer version 2.0.8 2020-12-03 17:20:38

Usage:
  command [options] [arguments]

Options:
  -h, --help                     Display this help message
  -q, --quiet                    Do not output any message
  -V, --version                  Display this application version
      --ansi                     Force ANSI output
      --no-ansi                  Disable ANSI output
  -n, --no-interaction           Do not ask any interactive question
      --profile                  Display timing and memory usage information
      --no-plugins               Whether to disable plugins.
  -d, --working-dir=WORKING-DIR  If specified, use the given directory as working directory.
      --no-cache                 Prevent use of the cache
  -v|vv|vvv, --verbose           Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  about                Shows the short information about Composer.

...etc.
```
<br />

<div align=center>

<img src="https://t3.ftcdn.net/jpg/02/33/38/08/360_F_233380815_4ah3nJszftXDiCch6CwlmlAJdJSsRlSM.jpg" width="800" loading="lazy" />>

</div>

**Nous voilà arrivée à la fin de la configuration de notre environnement PHP**.  
Si vous le souhaitez, vous pourrez aller jeter un coup d'oeil du côté des **frameworks**.

<br />

## Sommaire principal

> Je vous propose de revenir à la page d'**accueil** et de vous rendre dans la partie **framework**.  

> _**Si vous savez ce que vous faites, vous êtes libre d'aller ou vous le souhaitez**_.


| chapitre | technologie | ordre conseillé | lien | statut |
| :--------: | :--------: | :--------: | :--------: | :----: |
| I | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Apache_HTTP_server_logo_%282016%29.svg/1280px-Apache_HTTP_server_logo_%282016%29.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../01-apache2/readme.md) | :heavy_check_mark:
| II | <img src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/01/MariaDB_610.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../02-mariadb/readme.md) | :heavy_check_mark:
| III | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../03-php/readme.md) | :heavy_check_mark:
| IV | <img src="https://upload.wikimedia.org/wikipedia/commons/9/95/PhpMyAdmin_logo.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../04-phpmyadmin/readme.md) | :heavy_check_mark:
| V | <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Logo-composer-transparent.png" width="50" loading="lazy" />| :no_entry: | [:link:](../05-composer/readme.md) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**. 

