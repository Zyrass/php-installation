# Chapitre II - Installation de MariaDB

<br />

<div align="center">

<img src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/01/MariaDB_610.png" width="400" loading="lazy" >

</div>

<br />

**MariaDB est un système de gestion de base de données édité sous licence GPL**.  
**Il s'agit d'un fork communautaire de MySQL : la gouvernance du projet est assurée par la fondation MariaDB, et sa maintenance par la société Monty Program AB, créateur du projet.**.  
[source wikipédia](https://fr.wikipedia.org/wiki/MariaDB)

<br />

## Sommaire de Maria DB

| Etape | Description | Lien | Statut
| :--------: | -------- | :--------: | :------: |
| 2.1 | Installation de MariaDB | [:link:](#21-installation-de-mariadb) | :heavy_check_mark:
| 2.2 | Sécurisation de MariaDB | [:link:](#22-s%C3%A9curisation-de-mariadb) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**.

<br />

## 2.1 - Installation de MariaDB

```sh
# 2.1.1 - Installation de MariaDB (MySQL)
sudo apt install mariadb-server mariadb-client -y

# 2.1.2 - Démarrer automatiquement le service au démarrage de l'ordinateur
sudo systemctl enable mariadb

# 2.1.3 - Vérification de l'état du service de MariaDB.<br />
sudo systemctl status mariadb

# 2.1.4- Démarrer le service s'il n'est pas démarré.
sudo systemctl start mariadb

# 2.1.5 (Bonus) - Stopper le service "mariadb" 
sudo systemctl stop mariadb
```
À cet instant, **mariaDB est installé** sauf qu'**il n'est pas sécurisé,** on y vient juste après.


<br />

## 2.2 - Sécurisation de MariaDB

> Par défaut avec **mariadb**, vous vous connecterez avec votre **nom d'utilisateur** et avec votre **mot de passe** correspondant à votre système.  

```sh
# 2.2.1 - Configuration de mariaDB
sudo mysql_secure_installation

# Explication
# -------------------------------------------------------------------------
# À la 1re ligne, appuyée sur la touche "ENTREE" de votre clavier
# pour valider la saisie avec un mot de passe vide.
#
# Lorsque "Set root password?" s'affichera à l'écran, il vous sera demandé, de valider par la touche
# "Y" de votre clavier pour définir un mot de passe. (Obligatoire)
# /!\ Le mot de passe ne s'affichera pas et c'est tout à fait normal.
# Une confirmation vous sera demandée.
#
# À ce moment, une série de questions vous sera demandée, valider toutes les
# questions par la touche "Y".  
#
# -------------------------------------------------------------------------
# Ci-dessous les questions qui vous seront posées :
# -------------------------------------------------------------------------
#
# - Remove anonymous users?
#   - (Suppression des utilisateurs anonyme)
#
# - Disallow root login remotely?
#   - (Désactivez la connexion root à distance)
#
# - Remove test database and access to it?
#   - (Supprimer la base de données test)
#
# - Reload privilege tables now?
#   - (Recharger les privilèges des tables maintenant ?)
```

Une fois que tout sera fait, vous obtiendrez un message spécifiant que tout est OK.

    All done!  
    If you've completed all of the above steps, your MariaDB installation should now be secure.  

    Thanks for using MariaDB!

### Test de connexion :

```sh
# 2.2.2 - Test via le terminal pour se connecter en "root"
sudo mariadb -u root
```
    - Pour quitter "mariadb" on saisira "exit" en validant par la touche "ENTRER" du clavier.

<br />

## Sommaire principal

> Je vous propose de suivre le lien vers une **passerelle** ou vous retrouverez **différentes versions de PHP** à installer.  

> _**Si vous savez ce que vous faites, vous êtes libre d'aller ou vous le souhaitez**_.

| chapitre | technologie | ordre conseillé | lien | statut |
| :--------: | :--------: | :--------: | :--------: | :------: |
| I | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Apache_HTTP_server_logo_%282016%29.svg/1280px-Apache_HTTP_server_logo_%282016%29.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../01-apache2/readme.md) | :heavy_check_mark:
| II | <img src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/01/MariaDB_610.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../02-mariadb/readme.md) | :heavy_check_mark:
| III | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png" width="50" loading="lazy" /> | :thumbsup: | [:link:](../03-php/readme.md) | :heavy_check_mark:
| IV | <img src="https://upload.wikimedia.org/wikipedia/commons/9/95/PhpMyAdmin_logo.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../04-phpmyadmin/readme.md) | :heavy_check_mark:
| V | <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Logo-composer-transparent.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../05-composer/readme.md) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**.
