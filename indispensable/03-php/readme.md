# Passerelle vers les différentes installations de PHP

<br />

<div align=center>

<img src="https://9to5php.com/uploads/featured-php-8-0-0-released-cf8b3dcea17c4b06508f955096c5fbc0.png" width="800" loading="lazy" />

</div>

<br />

**PHP: Hypertext Preprocessor, plus connu sous son sigle PHP, est un langage de programmation libre,**  
**principalement utilisé pour produire des pages Web dynamiques via un serveur HTTP,**  
**mais pouvant également fonctionner comme n'importe quel langage interprété de façon locale.**  
**PHP est un langage impératif orienté objet.**  
[source wikipédia](https://fr.wikipedia.org/wiki/PHP)

<br />

## Sommaire - Choix d'une version de PHP

> Le chapitre III, se répète mais la configuration change selon **la version de php** qui aura été choisi.

| chapitre | description | lien | statut
| :--------: | -------- | :--------: | :-------: |
| 3 | Installations de **PHP 5+** | :heavy_multiplication_x: | __à faire__
| 3 | Installations de **PHP 7+** | [:link:](php7+/readme.md) | :heavy_check_mark:
| 3 | Installations de **PHP 8+** | [:link:](php8+/readme.md) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**. 

<br />

## Sommaire principal

> Je vous propose de suivre le lien vers **phpMyAdmin** pour continuer la configuration de votre environnement php.
  
> _**Si vous savez ce que vous faites, vous êtes libre d'aller ou vous le souhaitez**_.

| chapitre | technologie | ordre conseillé | lien | statut |
| :--------: | :--------: | :--------: | :--------: | :------|
| I | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Apache_HTTP_server_logo_%282016%29.svg/1280px-Apache_HTTP_server_logo_%282016%29.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../01-apache2/readme.md) | :heavy_check_mark:
| II | <img src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/01/MariaDB_610.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../02-mariadb/readme.md) | :heavy_check_mark:
| III | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../03-php/readme.md) | :heavy_check_mark:
| IV | <img src="https://upload.wikimedia.org/wikipedia/commons/9/95/PhpMyAdmin_logo.png" width="50" loading="lazy" /> | :thumbsup: | [:link:](../04-phpmyadmin/readme.md) | :heavy_check_mark:
| V | <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Logo-composer-transparent.png" width="50" loading="lazy" />| :no_entry: | [:link:](../05-composer/readme.md) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**. 
