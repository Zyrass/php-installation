# Chapitre III - Installation de PHP 7+

<br />

<div align=center>

<img src="https://gecka.nc/wp/wp-content/uploads/2020/02/php74.jpg" width="800" loading="lazy" />


**Ce guide est axé sur la version 7.4 de PHP**

</div>

<br />

## Sommaire de PHP 7.4

| chapitre | description | lien | statut
| :--------: | -------- | :--------: | :--------: |
| 3.1 | Installation de PHP 7.4 | [:link:](#31-installation-de-php-74) | :heavy_check_mark:
| 3.2 | Test du bon fonctionnement de PHP 7.4 | [:link:](#32-test-du-bon-fonctionnement-de-php-74) | :heavy_check_mark:
| 3.3 | Améliorer les performances de PHP | [:link:](#33-am%C3%A9liorer-les-performances-de-php) | :heavy_check_mark:
| 3.4 | Affichage des erreurs | [:link:](#34-affichage-des-erreurs) | :heavy_check_mark:
| 3.5 | Test si les erreurs sont visibles | [:link:](#35-test-si-les-erreurs-sont-visibles) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**. 


<br />

## 3.1 - Installation de PHP 7.4

```sh
# 3.1.1 - On met à jour notre gestionnaire de paquets.
sudo apt update && sudo apt full-upgrade && sudo apt autoremove

# 3.1.2 - Installation de PHP 7.4 et de tous ces modules.
sudo apt install php-common php7.4 libapache2-mod-php7.4 php7.4-mysql php7.4-cli php7.4-common php7.4-opcache php7.4-readline php7.4-curl php7.4-gd php7.4-intl php7.4-sqlite3 php7.4-gmp php7.4-mbstring php7.4-xml php7.4-zip php7.4-json 

# 3.1.3 - On active PHP via la commande suivante :
sudo a2enmod php7.4

# 3.1.4 - Redémarrage du service "apache2"
sudo systemctl restart apache2
```

<br />

## 3.2 - Test du bon fonctionnement de PHP 7.4

> Pour voir si les fichiers PHP sont bien pris en charge, on va créer un fichier **phpinfo.php**, lui définir un contenu,  
> le tout placé dans le répertoire " **/var/www/html/** ".

```sh
# 3.2.1 - Création du fichier et édition de celui-ci grâce à nano.
sudo nano /var/www/html/phpinfo.php
```

Dans l'éditeur (**nano**) qui vient de s'ouvrir, on ajoute la ligne PHP ci-dessous.

```php
<?php phpinfo();
```

    - Comme on l'a déjà fait, on sauvegarde avec la combinaison : CTRL + S
    - On quitte l'éditeur nano avec : CTRL + X

> On ouvre notre navigateur, on saisit l'URL : localhost/phpinfo.php.  
> Là, on va directement voir le contenu du fichier directement dans le navigateur.  
> Si ça fonctionne c'est que tout est bon pour le moment.


**/!\ Attention tout de même, il nous faudra supprimer ce fichier vu que c'est tout de même un peu dangereux de laisser toutes ces informations visibles par n'importe qui**.

    On supprimera donc celui-ci lors de l'affichage des erreurs. 

<br />

## 3.3 - Améliorer les performances de PHP

> On va utiliser **php-fpm** pour améliorer les performances de PHP.

```sh
# 3.3.1 - On désactive le module php7.4
sudo a2dismod php7.4

# 3.3.2 - On install php7.4-fpm
sudo apt install php7.4-fpm

# 3.3.3 - On active le module proxy et environnement. 
sudo a2enmod proxy_fcgi setenvif

# 3.3.4 - Enregistrement de la configuration
sudo a2enconf php7.4-fpm 

# 3.3.5 - On peut redémarrer le serveur apache2
sudo systemctl restart apache2
```

<br />

## 3.4 - Affichage des erreurs

**Par défaut, les erreurs ne sont pas affichées ce qui peut-être très regrettable lorsqu'on développera quoi que ce soit.**

> Dans notre fichier qu'on a créé dans les chapitres précédents,  
> nous allons faire en forte de vérifier une ligne importante qui devra être utilisée à l'étape **3.4.1** selon ce qu'il
> vous sera retourné.

### afficher à l'écran le fichier phpinfo.php

- Rechercher la ligne : **Loaded Configuration File**
    - Moi elle me retourne : **/etc/php/7.4/fpm/php.ini**

    Ainsi je devrais modifier ce **php.ini** situé dans le répertoire : **/etc/php/7.4/fpm/**

> Pour certain, vous remplacerez **fpm** par : **apache2** ou **cli**


```sh
# 3.4.1 - Édition du fichier "php.ini" pour PHP 7.4
sudo nano /etc/php/7.4/fpm/php.ini
```

### Contenu à modifier dans le fichier php.ini :
```sh
# 3.4.2 - Il nous faut décommenter quelques lignes.
#         Pour décommenter, on enlève juste le ";" soit :

 display_errors
   Default Value: On
   Development Value: On
   Production Value: Off

 display_startup_errors
   Default Value: On
   Development Value: On
   Production Value: Off

 error_reporting
   Default Value: E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED
   Development Value: E_ALL
   Production Value: E_ALL & ~E_DEPRECATED & ~E_STRICT

 log_errors
   Default Value: Off
   Development Value: On
   Production Value: On

#  3.4.3 - Lorsqu'on est en développement, on affichera toutes les erreurs.
#          Il faut donc modifier la ligne suivante : 
#              error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
#          par :
error_reporting = E_ALL

# 3.4.4 - Également, toujours dans ce même fichier, il faut modifier 
#         la valeur de : "display_errors = Off"
#         par :
display_errors = On
```

    On sauvegarde le fichier avec les touches suivantes : CTRL + S
    On quitte avec : CTRL + X

```sh
# 3.4.5 - On recharge le service php7.4-fpm afin de prendre en compte les changements
sudo service php7.4-fpm reload

# On redémarre le service "apache2"
sudo systemctl restart apache2
```

### Bonus - En production

Ci-dessous les lignes de code à réinsérer

    error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
    display_errors = Off

<br />

## 3.5 - Test si les erreurs sont visibles

Donc nous allons ici, **tester les erreurs**, histoire de voir si elles s'affichent bel et biens à l'écran.

```sh
# 3.5.1 - Modification du fichier "phpinfo.php" créé un peu plus tôt.
sudo nano /var/www/html/phpinfo.php
```

On cherche à modifier le code ci-dessous pour **provoquer une erreur** par celui juste après.

```php
<?php phpinfo(); # Code normalement correct (provoque aucune erreur)
```

```php
<?php phpinfo(; # Code incorrect (provoque une erreur)
```

### Erreur normalement générée à l'écran :

    Parse error: syntax error, unexpected ';' in /var/www/html/phpinfo.php on line 1

### Suppression de ce fichier qui peut être dangereux :

```sh
# 3.5.2 - Comme je l'ai dit à la fin en "3.2.1", 
#         On supprime ce fichier (phpinfo.php) par mesure de sécurité.
sudo rm -rf /var/www/html/phpinfo.php
```
<br />

## Sommaire principal

> Je vous propose de suivre le lien vers **phpMyAdmin** pour continuer la configuration de votre environnement PHP.
  
> _**Si vous savez ce que vous faites, vous êtes libre d'aller ou vous le souhaitez**_.

| chapitre | technologie | ordre conseillé | lien | statut |
| :--------: | :--------: | :--------: | :--------: | :--------: |
| I | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Apache_HTTP_server_logo_%282016%29.svg/1280px-Apache_HTTP_server_logo_%282016%29.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../../01-apache2/readme.md) | :heavy_check_mark:
| II | <img src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/01/MariaDB_610.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../../02-mariadb/readme.md) | :heavy_check_mark:
| III | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../readme.md) | :heavy_check_mark:
| IV | <img src="https://upload.wikimedia.org/wikipedia/commons/9/95/PhpMyAdmin_logo.png" width="50" loading="lazy" /> | :thumbsup: | [:link:](../../04-phpmyadmin/readme.md) | :heavy_check_mark:
| V | <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Logo-composer-transparent.png" width="50" loading="lazy" />| :no_entry: | [:link:](../../05-composer/readme.md) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**. 

 
