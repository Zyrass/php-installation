# Chapitre IV - Installation de phpMyAdmin

<br />

<div align=center>

<img src="https://community.jaguar-network.com/wp-content/uploads/2017/05/phpmyadmin-696x456.png.webp" width="800" />
<br />

**phpMyAdmin est une application Web de gestion pour les systèmes de gestion de base de données MySQL**  
**réalisée principalement en PHP et distribuée sous licence GNU GPL**.  
[source](https://fr.wikipedia.org/wiki/PhpMyAdmin)

</div>

<br />

## Sommaire de phpMyAdmin

| chapitre | description | lien | statut |
| :--------: | -------- | :--------: | :-------: |
| 4.1 | Avant de commencer | [:link:](#41-avant-de-commencer) | :heavy_check_mark:
| 4.2 | Installation de **phpMyAdmin** | [:link:](#42-installation-de-phpmyadmin) | :heavy_check_mark:
| 4.3 | Configuration de **phpMyAdmin** | [:link:](#43-configuration-de-phpmyadmin) | :heavy_check_mark:
| 4.4 | Création du compte admin sur **phpMyAdmin** | [:link:](#44-cr%C3%A9ation-du-compte-admin-sur-phpmyadmin) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**. 


<br />

## 4.1 - Avant de commencer

> Par défaut si on ne fait rien, le nom d'utilisateur sera **phpMyAdmin** et le mot de passe sera **celui qui aura été créer juste après**.  

Il faut donc connaître son **nom d'utilisateur**.  

> Par exemple, j'ouvre un nouveau terminal en saisissant le raccourci clavier **CTRL** + **ATL** + **T** et dans mon terminal, moi j'ai ceci:  
    
    zyrass@zyrass-dev:~$

Donc mon cas, mon nom d'utilisateur correspond à **zyrass**.  
Adaptez avec le vôtre.

```sh
# 4.1.1 - Il nous faut être propriétaire du dossier tout 
#         en faisant partie du groupe pour apache2
sudo chown -R zyrass /var/www/html

# Remplacer zyrass par votre non d'utilisateur.
```

<br />

## 4.2 - Installation de phpMyAdmin

Rien de très sorcier ici :

```sh
# 4.2.1 - Procédons à l'installation de "phpmyadmin"
sudo apt install phpmyadmin
```

<br />

## 4.3 - Configuration de phpMyAdmin

    /!\ Il faut être très attentif ici /!\

> Nous nous retrouvons avec une interface toute violette avec des tâches à exécuter.

Donc à ce moment, on appuie sur la touche **ESPACE** pour avoir un petit astérisque...  
Puis on appuie sur la touche **TAB** et on valide la sélection par la touche **ENTRER** de notre clavier.

**On devrait obtenir ce genre de rendu :**

    [*] apache2
    [ ] lighttpd

Après une courte installation de quelques paquets, il nous sera demandé s'il faut configurer la base de données ?  
On validera par la touche **ENTRER** sur **OUI**.

On nous demande le **mot de passe de connexion à MySQL** pour phpMyAdmin.  
Donc pour éviter des erreurs, **on saisira le même mot de passe qu'on a défini précédemment**.

On nous demande une confirmation.  
On saisira la confirmation puis, nous pourrons tester notre connexion.

> Testons la connexion à phpMyAdmin via cette URL : **localhost/phpmyadmin**.

    RAPPEL :
    -------------------------------------------------------------------
        - Le nom d'utilisateur par défaut est : phpMyAdmin
        - le mot de passe : Celui qui a été défini précédemment.

<br />

## 4.4 - Création du compte admin sur phpMyAdmin

> Vous comprendrez très vite que le nom d'utilisateur "**phpMyAdmin**" n'est pas top...  
> Allez on va changer ça.

```sh
# 4.4.1 - On se connecte en root à mysql
sudo mysql -u root

# 4.4.2 - On saisit la commande suivante pour créer un utilisateur "admin" 
#         "xxxxxxxxx" sera à remplacer par votre mot de passe)
create user admin@localhost identified by 'xxxxxxxxx';

# 4.4.3 - On saisit ces 2 lignes de commande pour lui attribuer
#         tous les privilèges et les valider.
grant all privileges on *.* to admin@localhost with grant option;

# 4.4.4 - Pour valider les privilèges, on oublie surtout pas cette commande :
flush privileges;
 
# 4.4.5 - On quitte "mysql" en saisissant :
exit
```

<br />

## Sommaire principal

> Je vous propose de suivre le lien vers **Composer** afin d'avoir une meilleure utilisation de la **POO**.


> _**Si vous savez ce que vous faites, vous êtes libre d aller ou vous le souhaitez**_.

| chapitre | technologie | ordre conseillé | lien | statut
| :--------: | :--------: | :--------: | :--------: | :-----: |
| I | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Apache_HTTP_server_logo_%282016%29.svg/1280px-Apache_HTTP_server_logo_%282016%29.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../01-apache2/readme.md) | :heavy_check_mark:
| II | <img src="https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/01/MariaDB_610.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../02-mariadb/readme.md) | :heavy_check_mark:
| III | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1280px-PHP-logo.svg.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../03-php/readme.md) | :heavy_check_mark:
| IV | <img src="https://upload.wikimedia.org/wikipedia/commons/9/95/PhpMyAdmin_logo.png" width="50" loading="lazy" /> | :no_entry: | [:link:](../04-phpmyadmin/readme.md) | :heavy_check_mark:
| V | <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Logo-composer-transparent.png" width="50" loading="lazy" /> | :thumbsup: | [:link:](../05-composer/readme.md) | :heavy_check_mark:

**Où bien, retourner sur la page d'accueil [ici](../../../README.md)**. 
