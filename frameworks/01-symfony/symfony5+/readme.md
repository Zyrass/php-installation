# Chapitre VI - Installation de Symfony 5+

<br />

<div align=center>

<img src="https://wallpaperaccess.com/full/3255047.jpg" width="800" loading="lazy" />


**Ce guide est axé sur la version 5.2 de Symfony**

</div>

<br />

## Sommaire de Symfony 5

| chapitre | description | lien | statut
| :--------: | -------- | :--------: | :--------: |
| 6.1 | Les prérequis obligatoires | [:link:](#61-les-pr%C3%A9requis-obligatoires) | :heavy_check_mark:
| 6.2 | Téléchargement de la cli **Symfony** | [:link:](#62-t%C3%A9l%C3%A9chargement-de-la-cli-symfony) | :heavy_check_mark:
| 6.3 | Redémarrer un nouveau terminal | [:link:](#63-red%C3%A9marrer-un-nouveau-terminal) | :heavy_check_mark:
| 6.4 | Au cas où si ça ne retourne rien ou une erreur | [:link:](#64-au-cas-o%C3%B9-si-%C3%A7a-ne-retourne-rien-ou-une-erreur) | :heavy_check_mark:
| 6.5 | Démarrer un nouveau projet. | [:link:](#65-d%C3%A9marrer-un-nouveau-projet) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**. 

<br />

## 6.1 - Les prérequis obligatoires

Sur la **[documentation officielle](https://symfony.com/doc/current/setup.html#technical-requirements)**, il est recommandé d'avoir au préalable installé **PHP 7.2.5**
auquel cas **Symfony 5 ne pourra s'installer**.


1. Comme je l'ai dis, en priorité, **avoir PHP 7.2.5** au minimum d'installer.
2. Avoir composer d'installer
3. Avoir un serveur Apache d'installer.

    En sommes, il faut avoir un environnement PHP fonctionnel.
<br />

## 6.2 - Téléchargement de la cli Symfony

Via **[ce lien](https://symfony.com/download)**, on va récupérer la commande ci-dessous.  
J'ai volontairement mis le lien au cas où, ou si la commande viendrait à changer.

```sh
# Cette commande est à coller directement dans le terminal
wget https://get.symfony.com/cli/installer -O - | bash
```

<br />

## 6.3 - Redémarrer un nouveau terminal

Comme il est précisé juste avant, **si on ouvre un nouveau terminal**,
la commande **Symfony** devrait retourner quelque chose.

### Ouvrir un nouveau terminal :

    Pour ouvrir un nouveau terminal, 
    on va utiliser le raccourci clavier suivant : CTRL + ALT + T

### On teste la commande Symfony

```sh
# 6.3.1 - Test de la commande symfony
symfony
```

**Si la commande nous retourne les informations concernant Symfony,**  
**c'est que l'installation a fonctionné avec succès.**  

_Si tout est OK, nous pouvons nous rendre à l'étape **6.5**_
_Sinon on continue avec l'étape suivante. Là **[6.4](#64-au-cas-o%C3%B9-si-%C3%A7a-ne-retourne-rien-ou-une-erreur)**_.

<br />

## 6.4 - Au cas où si ça ne retourne rien ou une erreur

À l'étape 6.2, dans ce qu'il a été généré, on voit juste avant la dernière ligne qui nous dit d'ouvrir un nouveau terminal, on nous propose 3 possibilités pour installer la commande.

### Rendu perso des 3 propositions qui m'ont été faites :

```sh
Use it as a local file:
    # On utilisera cette ligne juste ci-dessous
    /home/zyrass/.symfony/bin/symfony

Or add the following line to your shell configuration file:
    export PATH="$HOME/.symfony/bin:$PATH"

Or install it globally on your system:
    mv /home/zyrass/.symfony/bin/symfony /usr/local/bin/symfony
```

Donc on va se baser sur la toute **1re option** qui nous est proposée.  
Pour cela, on va ouvrir un fichier qui est **caché** et qui se nomme **bashrc**

    ATTENTION :  
    --------------------------------------------------------------------------------
        Il ne faudra surtout rien supprimer dans le fichier qui sera utilisé.  
        Vous risquez tout simplement de corrompre votre système Ubuntu.

Peu importe où nous nous trouvons, on saisira dans le terminal :

```sh
# On saisira dans le terminal :
sudo nano ~/.bashrc

# A la fin de ce fichier, on va y ajouter cette ligne de commande :
export symfony="/home/zyrass/.symfony/bin/symfony"
```

       - On sauvegarde le fichier via la combinaison de touches suivante : CTRl + S
       - On ferme ce nano avec la commande : CTRL + X

**Il nous faut relancer ce fichier**

```sh
# Pour relancer/redémarrer le fichier bashrc on saisira :
source ~/.bashrc

```

<br />

## 6.5 - Démarrer un nouveau projet

Comme vous le savez, **apache2** est configuré pour se lancer dans le répertoire : **/var/www/html**  
Donc on va s'y rendre et on lancera la commande qui est proposé dans **[la documentation officielle](https://symfony.com/doc/current/setup.html#technical-requirements)**.

```sh
# On se rend dans le répertoire souhaité.
cd /var/www/html

# On lance la commande pour générer un projet
symfony new my_project_name --full
```

### Rendu attendu :

      * Creating a new Symfony project with Composer
        (running /usr/local/bin/composer create-project symfony/website-skeleton /var/www/html/my_project_name  --no-interaction)

      * Setting up the project under Git version control
        (running git init /var/www/html/my_project_name)

       [OK] Your project is now ready in /var/www/html/my_project_name

### Voir le projet une fois générer :

```sh
# Se rendre dans le répertoire précédemment créer :
cd my_project_name

# lancer le serveur :
symfony serve
```

On a fini on peut se rendre ici : **[http://127.0.0.1:8000](http://127.0.0.1:8000)**



<br />

## Sommaire principal

> Le premier lien vous redirigera vers le choix d'une version de **Symfony** à installer.  
> Le second lien lui vous redirigera vers le choix d'une version de **Laravel** à installer.

| chapitre | technologie | lien | statut |
| :--------: | :--------: | :--------: | :--------: |
| VI | <img src="https://cdn.worldvectorlogo.com/logos/symfony.svg" width="50" loading="lazy" /> | [:link:](../readme.md) | :heavy_check_mark:
| VII | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Laravel.svg/1200px-Laravel.svg.png" width="50" loading="lazy" /> | :heavy_multiplication_x: | __*à faire*__

**Où bien, retourner sur la page d'accueil [ici](../../../README.md)**. 

 