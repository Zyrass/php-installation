# Passerelle vers les différentes installations de Symfony

<br />

<div align=center>

<img src="https://www.brainvire.com/wp/wp-content/uploads/2016/03/why-should-you-rely-on-symfony-for-php-web-application-development.jpg" width="800" loading="lazy" />

</div>

<br />

**Symfony est un ensemble de composants PHP ainsi qu'un framework MVC libre écrit en PHP.**  
**Il fournit des fonctionnalités modulables et adaptables qui permettent de faciliter et d’accélérer le développement d'un site web.**  
**[source wikipédia](https://fr.wikipedia.org/wiki/Symfony)**

<br />

## Sommaire - Choix d'une version de Symfony

> Le premier chapitre, se répète mais la configuration change selon **la version de symfony** qui aura été choisi.

| chapitre | description | lien | statut
| :--------: | -------- | :--------: | :-------: |
| VI | Installation de **Symfony 4+** | :heavy_multiplication_x: | __à faire__
| VI | Installation de **Symfony 5+** | [:link:](symfony5+/readme.md) | :heavy_check_mark:

**Ou bien, retourner sur la page d'accueil [ici](../../../README.md)**. 

<br />

